# Simulación de transmisión de virus en humanos

Programa en C que simula la transmisión de virus en humanos.
Basado en [este programa](http://netlogoweb.org/launch#http://netlogoweb.org/assets/modelslib/Sample%20Models/Biology/Virus.nlogo "Ingresar para ver el programa base.") en NetLogo.

## Requerimientos
* Ubuntu 18.04 en adelante
* gcc
* matplotlib

## Uso

Luego de compilar con:

```bash
make
```
Se corre:

```bash
./sim_virus  -np < * > -i < * > -cr < * > -d < * > -w < * >  -p
```

La simulación es representada gráficamente con:
```bash
make graph
```

Donde cada argumento corresponde a:
* Número de personas en el mundo (10 a 300) (-np)
* Probabilidad de infección (0 a 99) (-i)
* Probabilidad de recuperación (0 a 99) (-cr)
* Tiempo en que el virus es transmisible, en semanas (0 a 99) (-d)
* Tiempo de simulación, en semanas (-w)

Si se desea correr en paralelo con pthreads se pone -p, si no, se omite.

### Ejemplo de uso

Con los siguientes parámetros:

```bash
./sim_virus  -np 150 -i 50 -cr 50 -d 20 -w 1000 -p
```
Se obtienen los siguientes resultados:

![Ejemplo de resultados](example.png)