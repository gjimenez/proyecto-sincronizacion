#ifndef P2_H
#define P2_H

#include <stdio.h>
#include <stdbool.h>
#include "P1.h"

#define N 35 // Dimensiones del mundo
#define INI_INFECTED 10 // Dimension del tablero
#define CAPACITY 300 // Dimension del tablero

void setup_p(int number_people, int infectiousness, int chance_recover, int duration, human humans[CAPACITY]);

void update_global_variables_p(human humans[CAPACITY]);

void simulate_p(human humans[CAPACITY], int weeks);

void *update_p(void* args);

void *week_p(void* args);

void *healthy_p(void* human_args);

void *first_cases(void* human_args);

void *setuph_p(void* human_args);

void setup_humans_p(human humans[CAPACITY]);


void *infectsubrutine_p(void* args);

void infect_p(human humans[CAPACITY], int pos_x, int pos_y);



#endif
