#ifndef P1_H
#define P1_H

#include <stdio.h>
#include <stdbool.h>

#define N 35 // Dimensiones del mundo
#define INI_INFECTED 10 // Dimension del tablero
#define CAPACITY 300 // Dimension del tablero

typedef struct
{
    int healthy;
    int infected;
    int immune;

    float prc_healthy;
    float prc_infected;
    float prc_immune;

    int lifespan;
    int chance_reproduce;
    int carrying_capacity;
    int immunity_duration;

    int number_people;
    int infectiousness;
    int chance_recover;
    int duration;
}globals;

typedef struct
{
    int pos_x;
    int pos_y;
    bool sick;
    bool dead;
    int remaining_immunity;
    int sick_time;
    int age;
}human;


void setup(int number_people, int infectiousness, int chance_recover, int duration, human humans[CAPACITY]);

void setup_constants(int number_people, int infectiousness, int chance_recover, int duration);

void setup_humans(human humans[CAPACITY]);

void update_global_variables(human humans[CAPACITY]);

void simulate(human humans[CAPACITY], int weeks);

void get_older(human* human_p);

void move(human* human_p);

void recover_or_die(human* human_p);

void die(human* human_p);

void infect(human humans[CAPACITY], int pos_x, int pos_y);

void reproduce(human humans[CAPACITY], int pos_x, int pos_y);

void become_immune(human* human_p);

void get_sick(human* human_p);

void get_healthy(human* humanP);

void write_to_file(FILE* fptr, int week);




#endif
