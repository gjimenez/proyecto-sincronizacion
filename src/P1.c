#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "P1.h"

globals world_variables;

void setup(int number_people, int infectiousness, int chance_recover, int duration, human humans[CAPACITY]){

    setup_constants(number_people, infectiousness, chance_recover, duration);
    setup_humans(humans);

    // printf("%d\n", world_variables.healthy);
    // printf("%d\n", world_variables.infected);
    // printf("%d\n", world_variables.immune);
    // printf("%f\n", world_variables.prc_healthy);
    // printf("%f\n", world_variables.prc_infected);
    // printf("%f\n", world_variables.prc_immune);
    // printf("%d\n", humans[0].pos_x);
    // printf("%d\n", humans[0].pos_y);
}

void setup_constants(int number_people, int infectiousness, int chance_recover, int duration){
    world_variables.healthy = number_people - INI_INFECTED;
    world_variables.infected = INI_INFECTED;
    world_variables.immune = 0;

    world_variables.lifespan = 50 * 52; //2600 semanas = 50 años
    world_variables.chance_reproduce = 1;
    world_variables.carrying_capacity = CAPACITY;
    world_variables.immunity_duration = 52;

    world_variables.number_people = number_people; // El mundo empieza con esta cantidad de personas
    world_variables.infectiousness = infectiousness;
    world_variables.chance_recover = chance_recover;
    world_variables.duration = duration;

    world_variables.prc_healthy = (float) world_variables.healthy / (float) world_variables.number_people * 100.0;
    world_variables.prc_infected = (float) world_variables.infected / (float) world_variables.number_people * 100.0;
    world_variables.prc_immune = (float) world_variables.immune / (float) world_variables.number_people * 100.0;
}

void setup_humans(human humans[CAPACITY]){

    for(int i = 0; i < world_variables.number_people; i++){
        // Estos estan vivos
        humans[i].dead = false;
        // Determina una posicion aleatoria entre 0 y N-1
        humans[i].pos_x = rand() % N;
        humans[i].pos_y = rand() % N;

        // Infecta a los primeros 10 humanos
        if(i < 10){
            humans[i].sick = true;
        }
        else{
            humans[i].sick = false;
        }

        humans[i].remaining_immunity = 0;
        humans[i].sick_time = 0;

        // Cada persona empieza con una edad de entre 0 a 2600 semanas
        humans[i].age = rand() % world_variables.lifespan;
    }
    for(int i = world_variables.number_people; i < CAPACITY; i++){
        humans[i].dead = true;
    }
}

void update_global_variables(human humans[CAPACITY]){
    world_variables.healthy = 0;
    world_variables.infected = 0;
    world_variables.immune = 0;

    // Se recorre el mundo buscando personas enfermas, sanas e inmunes (que no estén muertas)
    for(int i = 0; i < CAPACITY; i++){
        if(!humans[i].dead){
            if(humans[i].sick){
                world_variables.infected++;
            }
            else{
                if(humans[i].remaining_immunity > 0){
                    world_variables.immune++;
                }
                else{
                    world_variables.healthy++;
                }
            }
        }
    }

    // Actualiza los porcentajes
    if(world_variables.number_people != 0){
        world_variables.prc_healthy = (float) world_variables.healthy / (float) world_variables.number_people * 100.0;
        world_variables.prc_infected = (float) world_variables.infected / (float) world_variables.number_people * 100.0;
        world_variables.prc_immune = (float) world_variables.immune / (float) world_variables.number_people * 100.0;
    } else{
        world_variables.prc_healthy = 0.0;
        world_variables.prc_infected = 0.0;
        world_variables.prc_immune = 0.0;
    }

}

void simulate(human humans[CAPACITY], int weeks){
    FILE *fptr;

    update_global_variables(humans);
    write_to_file(fptr, 0);

    /*  Simulacion principal por semana (tic)
        En cada tic los humanos:
        - Envejecen
        - Mueven
        - Los infectados o se vuelven inmunes o se mueren
        - Los infectados infectan a otros que se encuentran en el mismo espacio
        - Los sanos se reproducen
     */
    for(int week = 0; week < weeks; week++){ // No sé hasta cuando se pide simular
        for(int i = 0; i < CAPACITY; i++){
            if(!humans[i].dead){
                get_older(&humans[i]);

                move(&humans[i]);

                if(humans[i].sick){
                    recover_or_die(&humans[i]);
                }

                // Hay que revisar si no se murió
                if(!humans[i].dead){
                    if(humans[i].sick){
                        infect(humans, humans[i].pos_x, humans[i].pos_y);
                    }else{
                        reproduce(humans, humans[i].pos_x, humans[i].pos_y);
                    }
                }
            }
        }

        // Se actualizan las variables y globales y la bitácora en cada tic
        update_global_variables(humans);
        write_to_file(fptr, week+1);
        // printf("Healthy = %d\n", world_variables.healthy);
        // printf("Infected = %d\n", world_variables.infected);
        // printf("Immune = %d\n", world_variables.immune);
    }

    //printf("%d\n", world_variables.number_people);
}

void get_older(human* human_p){
    //Incrementa su edad
    human_p->age++;

    // Incrementa tiempo de infectado o disminuye tiempo de inmunidad
    if(human_p->sick){
        human_p->sick_time++;
    }
    else{
        if(human_p->remaining_immunity > 0){
            human_p->remaining_immunity--;
        }
    }

    // Si el humano ya tiene más de su esperanza de vida, se muere
    if(human_p->age > world_variables.lifespan){
        die(human_p);
    }
}

void move(human* human_p){
    int num_aleatorio = rand();

    //printf("POS X ORIGINAL = %d\n", human_p->pos_x);
    //printf("POS Y ORIGINAL = %d\n", human_p->pos_y);

    // Movimiento aleatorio de un paso
    if(num_aleatorio % 2 == 0){
        // El mundo es circular
        if(human_p->pos_x == N-1){
            human_p->pos_x = 0;
        }else{
            human_p->pos_x++;
        }
    }else{
        if(human_p->pos_x == 0){
            human_p->pos_x = N-1;
        }else{
            human_p->pos_x--;
        }
    }

    // Se obtiene otro numero aleatorio a partir del original
    num_aleatorio /= 2;

    if(num_aleatorio % 2 == 0){
        // El mundo es circular
        if(human_p->pos_y == N-1){
            human_p->pos_y = 0;
        }else{
            human_p->pos_y++;
        }
    }else{
        if(human_p->pos_y == 0){
            human_p->pos_y = N-1;
        }else{
            human_p->pos_y--;
        }
    }

    //printf("POS X LUEGO = %d\n", human_p->pos_x);
    //printf("POS Y LUEGO = %d\n\n", human_p->pos_y);

}

void recover_or_die(human* human_p){
    // Cuando un humano está enfermo por suficiente tiempo, se vuelve inmune o se muere
    if(human_p->sick_time > world_variables.duration){
        if(rand() % 100 < world_variables.chance_recover){ // Se vuelve inmune con una cierta probabilidad chance_recover
            become_immune(human_p);
        }else{
            die(human_p);
        }
    }
}

void die(human* human_p){
    human_p->dead = true;
    world_variables.number_people--;
}

void infect(human humans[CAPACITY], int pos_x, int pos_y){
    for(int i = 0; i < CAPACITY; i++){
        if(!humans[i].dead){
            // Si comparte espacio con el infectado y no está inmune, se infecta según la probabilidad
            if((!humans[i].sick) && (humans[i].remaining_immunity <= 0) && (humans[i].pos_x == pos_x) && (humans[i].pos_y == pos_y)){ // Me tomo 1 hora encontrar que puse "=" en vez de "==" :))))))))))
                if(rand() % 100 < world_variables.infectiousness){ // Se infecta con una cierta probabilidad infectiousness
                    get_sick(&humans[i]);
                }
            }
        }
    }

}

void reproduce(human humans[CAPACITY], int pos_x, int pos_y){
    if((world_variables.number_people < CAPACITY) && (rand() % 100 < world_variables.chance_reproduce)){
        for(int i = 0; i < CAPACITY; i++){
            if(humans[i].dead){ // Busca el primer muerto, para reemplazarlo
                humans[i].dead = false; // Lázaro!

                // Misma posicion que la madre
                humans[i].pos_x = pos_x;
                humans[i].pos_y = pos_y;
                humans[i].age = 1;
                get_healthy(&humans[i]);
                world_variables.number_people++;
                break;
            }
        }
    }

}

void become_immune(human* human_p){
    human_p->sick = false;
    human_p->sick_time = 0;
    human_p->remaining_immunity = world_variables.immunity_duration;
}

void get_sick(human* human_p){
    human_p->sick = true;
    human_p->remaining_immunity = 0;
}

void get_healthy(human* human_p){
    human_p->sick = false;
    human_p->remaining_immunity = 0;
    human_p->sick_time = 0;
}

void write_to_file(FILE* fptr, int week){

    if(week == 0){
        // Se guarda en el directorio base
        fptr = fopen("sim_data.csv","w");

        if(fptr == NULL){
            printf("An error has ocurred when writing to file.");
            exit(EXIT_FAILURE);
        }
        fprintf(fptr, "%s,%s,%s,%s,%s,%s,%s,%s\n", "Week", "Sick", "Healthy", "Immune", "Total", "%Infected", "%Immune", "Years");

    }else{
        // Se guarda en el directorio base
        fptr = fopen("sim_data.csv","a");

        if(fptr == NULL){
            printf("An error has ocurred when writing to file.");
            exit(EXIT_FAILURE);
        }
    }

    fprintf(fptr, "%d,%d,%d,%d,%d,%f,%f,%f\n",  week,
                                                world_variables.infected,
                                                world_variables.healthy,
                                                world_variables.immune,
                                                world_variables.number_people,
                                                world_variables.prc_infected,
                                                world_variables.prc_immune,
                                                (float) week/52.1429);
    fclose(fptr);
}
