import matplotlib.pyplot as plt
import csv

colors = ['r', 'g', 'b', 'k']

with open('sim_data.csv','r') as csvfile:
    plots = list(csv.reader(csvfile, delimiter=','))
    legends = plots[0]                                              # Legends son la primera fila de la matriz
    plots = plots[1:]
    plots = [[float(column) for column in rows] for rows in plots]  # Convierte los datos a floats
    plots = list(map(list, zip(*plots)))                            # Transpone la matriz con los datos


for i in range(1, 5):
    plt.plot(plots[0], plots[i], colors[i - 1], label = legends[i], linewidth=0.5)  # Grafica los datos

plt.xlabel("Weeks")
plt.ylabel("People")
plt.title('Virus Transmission Simulation')
plt.legend(loc=1)
axes = plt.gca()
axes.yaxis.grid()   # Lineas horizontales
plt.show()