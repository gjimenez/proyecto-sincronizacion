#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>
#include <P1.h>
#include <P2.h>

int main(int argc, char *argv[]) {

    /* ARGUMENTOS */

    uint16_t number_people, infectiousness, chance_recover, duration, weeks;
    bool parallel = false;

    for (size_t i = 0; i < argc; i++) {
        // Revisión simple de suficientes argumentos
        if (argc < 11) {
            printf("Faltan argumentos");
            exit (EXIT_FAILURE);
        }
        // Número de personas
        if (strcmp(argv[i], "-np") == 0) {
            number_people = atoi(argv[i+1]);
            assert((number_people >= INI_INFECTED) && (number_people < CAPACITY));
        }
        // Infectividad
        else if (strcmp(argv[i], "-i") == 0) {
            infectiousness = atoi(argv[i+1]);
            assert((infectiousness >= 0) && (infectiousness < 100));
        }
        // Probabilidad de recuperacion
        else if (strcmp(argv[i], "-cr") == 0) {
            chance_recover = atoi(argv[i+1]);
            assert((chance_recover >= 0) && (chance_recover < 100));
        }
        // Duracion en que la persona puede infectar a otros
        else if (strcmp(argv[i], "-d") == 0) {
            duration = atoi(argv[i+1]);
            assert((duration >= 0) && (duration < 100));
        }
        // Duracion total de la simulacion
        else if (strcmp(argv[i], "-w") == 0) {
            weeks = atoi(argv[i+1]);
            assert((weeks >= 0));
        }
        // Si se quiere correr el programa con pthreads
        else if (strcmp(argv[i], "-p") == 0) {
            parallel = true;
        }
    }

    /* INICIALIZACIÓN */

    human humans[CAPACITY];
    // Tiempo actual como seed
    srand(time(0));

    /* SETUP Y SIMULACIÓN */
    if(!parallel){
        setup(number_people, infectiousness, chance_recover, duration, humans);
        simulate(humans, weeks);
    }
    else {
        /* SETUP Y SIMULACIÓN PARALELA*/
        setup_p(number_people, infectiousness, chance_recover, duration, humans);
        simulate_p(humans, weeks);
    }

    return 0;
}
