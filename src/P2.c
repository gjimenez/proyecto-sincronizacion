#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <assert.h>
#include "P2.h"


typedef struct{ //Struct con un solo humano
  int i;
  human human1;
  int pos_x;
  int pos_y;

}datos_thread;

typedef struct{ //Struct que tiene human con puntero
  int i;
  human *human1;
  int pos_x;
  int pos_y;
}datos_thread3;
//Definición de los mutex necesarios
pthread_mutex_t mutex1;
pthread_mutex_t mutex2;
pthread_mutex_t mutex3;

globals world_variables;

void setup_p(int number_people, int infectiousness, int chance_recover, int duration, human humans[CAPACITY]){
    setup_constants(number_people, infectiousness, chance_recover, duration);
    setup_humans_p(humans);
}
//Subrutina para la actualización de variables globales. Se utiliza mutex para proteger las variables
void *update_p(void* args){
  human *humans;
  humans = (human *) args;
  if(!humans->dead){
    if(humans->sick){
          pthread_mutex_lock (&mutex1);
          world_variables.infected++;
          pthread_mutex_unlock (&mutex1);
    }
    else{
        if(humans->remaining_immunity > 0){
            pthread_mutex_lock (&mutex2);
            world_variables.immune++;
            pthread_mutex_unlock (&mutex2);
        }
        else{
            pthread_mutex_lock (&mutex3);
            world_variables.healthy++;
            pthread_mutex_unlock (&mutex3);
        }
    }
  }
  pthread_exit(NULL);
}
//Función que actualiza las variables globales por medio de llamados a la función update_p por medio de los threads
void update_global_variables_p(human humans[CAPACITY]){
    world_variables.healthy = 0;
    world_variables.infected = 0;
    world_variables.immune = 0;
    //Inicialización de los mutex
    pthread_mutex_init(&mutex1, NULL);
    pthread_mutex_init(&mutex2, NULL);
    pthread_mutex_init(&mutex3, NULL);

    int NUM_THREADS = 12;
    int cont = 0;
    int cont2 = 0;
    int result_code;

    int veces_for = CAPACITY/NUM_THREADS;
    //Ciclo que recorre veces_for para completar todas las iteraciones (CAPACITY veces en total, incluyendo todos los hilos)
    for(cont2 = 0; cont2 < veces_for; cont2++){

      pthread_t threads[NUM_THREADS];

      int lim = cont2*NUM_THREADS;
      //Crea los threads
      for(cont = 0; cont < NUM_THREADS; cont++){
        result_code = pthread_create(&threads[cont], NULL, update_p, (void *) &humans[cont+lim]);
        assert(!result_code);
      }
      //Une los threads
      for(cont = 0; cont < NUM_THREADS; cont++){
        result_code = pthread_join(threads[cont], NULL);
        assert(!result_code);
      }

    }
    //Destruye los mutex
    pthread_mutex_destroy(&mutex1);
    pthread_mutex_destroy(&mutex2);
    pthread_mutex_destroy(&mutex3);


    // Actualiza los porcentajes
    if(world_variables.number_people != 0){
        world_variables.prc_healthy = (float) world_variables.healthy / (float) world_variables.number_people * 100.0;
        world_variables.prc_infected = (float) world_variables.infected / (float) world_variables.number_people * 100.0;
        world_variables.prc_immune = (float) world_variables.immune / (float) world_variables.number_people * 100.0;
    } else{
        world_variables.prc_healthy = 0.0;
        world_variables.prc_infected = 0.0;
        world_variables.prc_immune = 0.0;
    }

}

void *dead_p(void* human_args){ //Subrutina para los humanos que no forman parte de los vivos aun

  human *human1;
  human1 = (human *) human_args;
  human1->dead = true; //Simplemente se activa el bit de true
}

void *healthy_p(void* human_args){ //Subrutina para el caso de los que no estan infectados desde el inicio

  human *human1;

  human1 = (human *) human_args;
    // Estos estan vivos
    human1->dead = false;
    // Determina una posicion aleatoria entre 0 y N-1
    human1->pos_x = rand() % N;
    human1->pos_y = rand() % N;
    human1->sick = false;
    human1->remaining_immunity = 0;
    human1->sick_time = 0;

    // Cada persona empieza con una edad de entre 0 a 2600 semanas
    human1->age = rand() % world_variables.lifespan;

}


void *first_cases(void* human_args){//Subrutina para los primeros 10 casos donde estan infectados
  printf("entra\n");
  human *human1;

  human1 = (human *) human_args;

    human1->dead = false;
    // Determina una posicion aleatoria entre 0 y N-1
    human1->pos_x = rand() % N;
    human1->pos_y = rand() % N;

    // Infecta a los primeros 10 humanos
    human1->sick = true;

    human1->remaining_immunity = 0;
    human1->sick_time = 0;

    // Cada persona empieza con una edad de entre 0 a 2600 semanas
    human1->age = rand() % world_variables.lifespan;
}


void setup_humans_p(human humans[CAPACITY]){ //Se establecen las condiciones iniciales para los humanos

  int NUM_THREADS = 12;
  int cont = 0;
  int cont2 = 0;
  int i = 0;
  int result_code;

  pthread_t threads[NUM_THREADS];


  for(cont2 = 0; cont2 < CAPACITY/NUM_THREADS; cont2++){

    pthread_t threads[NUM_THREADS];

    int lim = cont2*NUM_THREADS;

    for(cont = 0; cont < NUM_THREADS; cont++){

      if (cont+lim<10) {//primeros 10 son contagiados
        result_code = pthread_create(&threads[cont], NULL, first_cases, (void *) &humans[cont+lim]);
        assert(!result_code);
      }else if ((cont+lim)<world_variables.number_people){//desde los 10 hasta la cantidad de personas iniciales
        result_code = pthread_create(&threads[cont], NULL, healthy_p, (void *) &humans[cont+lim]);
        assert(!result_code);
      } else {//Los "muertos"
        result_code = pthread_create(&threads[cont], NULL, dead_p, (void *) &humans[cont+lim]);
        assert(!result_code);
      }


    }

    for(cont = 0; cont < NUM_THREADS; cont++){
      result_code = pthread_join(threads[cont], NULL);
      assert(!result_code);
    }
  }
}



void simulate_p(human humans[CAPACITY], int weeks){
    FILE *fptr;

    update_global_variables_p(humans);

    write_to_file(fptr, 0);

    /*  Simulacion principal por semana (tic)
        En cada tic los humanos:
        - Envejecen
        - Mueven
        - Los infectados o se vuelven inmunes o se mueren
        - Los infectados infectan a otros que se encuentran en el mismo espacio
        - Los sanos se reproducen
     */
    for(int week = 0; week < weeks; week++){ // No sé hasta cuando se pide simular
        for(int i = 0; i < CAPACITY; i++){
            if(!humans[i].dead){
                get_older(&humans[i]);

                move(&humans[i]);

                if(humans[i].sick){
                    recover_or_die(&humans[i]);
                }

                // Hay que revisar si no se murió
                if(!humans[i].dead){
                    if(humans[i].sick){
                        infect_p(humans, humans[i].pos_x, humans[i].pos_y);
                    }else{
                        reproduce(humans, humans[i].pos_x, humans[i].pos_y);
                    }
                }
            }
        }

        // Se actualizan las variables y globales y la bitácora en cada tic
        update_global_variables_p(humans);
        write_to_file(fptr, week+1);
    }

}



void *infectsubrutine_p(void* args){ //Subrutina que decide si se infecta o no la persona
  datos_thread3 *datos1;

  datos1 = (datos_thread3 *) args;

  if(!datos1->human1->dead){
      // Si comparte espacio con el infectado y no está inmune, se infecta según la probabilidad
      if((!datos1->human1->sick) && (datos1->human1->remaining_immunity <= 0) && (datos1->human1->pos_x == datos1->pos_x) && (datos1->human1->pos_y == datos1->pos_y)){
          if(rand() % 100 < world_variables.infectiousness){ // Se infecta con una cierta probabilidad infectiousness

              datos1->human1->sick = true;
              datos1->human1->remaining_immunity = 0;
          }
      }
  }

}


void infect_p(human humans[CAPACITY], int pos_x, int pos_y){ //Metodo que recibe humans y utiliza infectsubrutine_p para decidir la infección
  int NUM_THREADS = 12;
  int cont = 0;
  int cont2 = 0;
  int i = 0;
  int result_code;

  datos_thread3 datos_arg[CAPACITY];
  //Se llenan los datos necesarios para pasar como parámetro a la subrutina
  for(i = 0; i < CAPACITY; i++){
    datos_arg[i].human1 = &humans[i];
    datos_arg[i].i = i;
    datos_arg[i].pos_x=pos_x;
    datos_arg[i].pos_y=pos_y;
  }


  //Ciclo que recorre veces_for para completar todas las iteraciones (CAPACITY veces en total, incluyendo todos los hilos)
  for(cont2 = 0; cont2 < CAPACITY/NUM_THREADS; cont2++){

    pthread_t threads[NUM_THREADS];

    int lim = cont2*NUM_THREADS;
    //Se crean los hilos
    for(cont = 0; cont < NUM_THREADS; cont++){

      result_code = pthread_create(&threads[cont], NULL, infectsubrutine_p, (void *) &datos_arg[cont+lim]);
      assert(!result_code);
    }
    //Se unen los hilos
    for(cont = 0; cont < NUM_THREADS; cont++){
      result_code = pthread_join(threads[cont], NULL);
      assert(!result_code);
    }

  }


}
