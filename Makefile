IDIR=include
SDIR=src
CC=gcc
CFLAGS=-I$(IDIR)
PYTHON=/usr/bin/python3

ODIR=obj

LIBS=-lm

_DEPS = P1.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_DEPS2 = P2.h
DEPS2 = $(patsubst %,$(IDIR)/%,$(_DEPS2))

_OBJ = main.o P1.o P2.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

sim_virus: $(OBJ)
	$(CC) -pthread -o $@ $^ $(CFLAGS) $(LIBS)

graph:
	$(PYTHON) $(SDIR)/P3.py

.PHONY: clean

clean:
	rm -f $(ODIR)/*.o sim_virus *.csv
